<?php

use Stagehand\Greeting;

class SimpleTest extends PHPUnit_Framework_TestCase
{
    public function testHello() {
        $msg="Konnichiwa";
        $greeting = new Greeting( $msg );
        $this->assertEquals($msg, $greeting->sayHello());
    }
}
