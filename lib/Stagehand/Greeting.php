<?php
namespace Stagehand;

class Greeting
{
    protected
        $hello;

    function __construct( $hello='Hello')
    {
        $this->hello = $hello;
    } 
    public function sayHello()
    {
        return $this->hello; 
    }
}
