# This is a sample php library 

# Install phpunit

install Composer:


    $ curl -s http://getcomposer.org/installer | php                                                                    

    #!/usr/bin/env php
    All settings correct for using Composer
    Downloading...
    
    Composer successfully installed to: /home/hdknr/php/stagehand/composer.phar
    Use it: php composer.phar

edit composer.json:

    {
        "require-dev": {
            "phpunit/phpunit": "3.7.*"
        },
        "autoload": {
            "psr-0": { "": "lib/" }
        }
    }

install:

    $ php composer.phar install

    Loading composer repositories with package information
    Installing dependencies (including require-dev)
      - Installing symfony/yaml (v2.4.0)
        Loading from cache
    
      - Installing phpunit/php-text-template (1.1.4)
        Loading from cache
    
      - Installing phpunit/phpunit-mock-objects (1.2.3)
        Loading from cache
    
      - Installing phpunit/php-timer (1.0.5)
        Loading from cache
    
      - Installing phpunit/php-token-stream (1.2.1)
        Loading from cache
    
      - Installing phpunit/php-file-iterator (1.3.4)
        Loading from cache
    
      - Installing phpunit/php-code-coverage (1.2.13)
        Loading from cache
    
      - Installing phpunit/phpunit (3.7.28)
        Loading from cache
    
    phpunit/php-code-coverage suggests installing ext-xdebug (>=2.0.5)
    phpunit/phpunit suggests installing phpunit/php-invoker (>=1.1.0,<1.2.0)
    Writing lock file
    Generating autoload files

phpunit.xml:


    $ cp phpunit.xml.dist  phpunit.xml
    $ vi phpunit.xml

run:

    $ vendor/phpunit/phpunit/phpunit.php 

    PHPUnit 3.7.28 by Sebastian Bergmann.
    
    Configuration read from /home/hdknr/php/stagehand/phpunit.xml
    
    Time: 32 ms, Memory: 2.50Mb
    
    OK (1 test, 1 assertion)


# Use Stagehand

configure your composer.json:


    {
        "repositories": [
            {
                "type": "vcs",
                "url": "https://bitbucket.org/hdknr/stagehand.git"
            }
        ],
        "require":{
            "hdknr/stagehand":"dev-master"
        }
    }

update( or install ) your composer:


    $ php composer.phar update
